Practica 2: Markdown y herramientas de programación colaborativas
Parte 1. Git y Markdown
Comprensión:
-Definiciones:
1) software de control de versiones: “Es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo de tal manera que sea posible recuperar versiones especificas más adelante.” (https://medium.com/@jointdeveloper/sistemas-de-control-de-versiones-qu%C3%A9-son-y-por-qu%C3%A9-amarlos-24b6957e716e)
2) Repositorios: “Son archivos donde se almacenan recursos digitales de manera que estos pueden ser accesibles a través de internet.”( https://poliscience.blogs.upv.es/open-access/repositorios/definicion-y-tipos/). 
Que un repositorio sea público o privado se refiere a la gente la cual tiene acceso a tu trabajo, si es publico todos tienen acceso y pueden verlo y aportar a el y si es privado solo las personas con la contraseña podrán.
3) Arboles de Merkle:” Es una estructura de datos estratificada que tiene como finalidad relacionar cada nodo con una raíz única asociada a éste”. ( https://agorachain.org/arboles-merkle-usos/)
“Los árboles de Merkle en Git nos permiten crear una especie de máquina del tiempo, que nos permite navegar por los distintos commit del proyecto, manteniendo los archivos y sus cambios tal como estaban en ese determinado momento” (https://agorachain.org/arboles-merkle-usos/)
4) Copia local y copia remota de los datos: Una copia local de datos es un conjunto de datos que se guardan en el dispositivo en el que estés trabajando y una copia remota de datos se refiere a cuando guardas todos esos archivos en una nube.
5) Commit:”Es la acción de guardar o subir tus archivos a tu repositorio remoto (una actualización de tus cambios) también puede hacerse al local (depende donde hayas creado tu repositorio).” (https://codigofacilito.com/articulos/commits-administrar-tu-repositorio) 
Debería hacerlo antes de cada tarea  que se quiera realizar.
6) como volvería a una versión anterior de mi trabajo: “En caso de que agregamos un archivo al índice de git y luego nos damos cuenta que este archivo no debe pasar, por ejemplo porque es un archivo de configuración o por que el cambio debe incluirse en una rama diferente, entonces podemos usar el comando git reset head, seguido por el nombre del archivo que no deseemos incluir”. (https://www.codigonaranja.com/2017/como-revertir-cambios-en-git-o-regresar-a-una-version-anterior)
7) Ramas: “Las ramas son caminos que puede tomar el desarrollo de un software, algo que ocurre naturalmente para resolver problemas o crear nuevas funcionalidades. 

En la práctica permiten que nuestro proyecto pueda tener diversos estados y que los desarrolladores sean capaces de pasar de uno a otro de una manera ágil.” (https://desarrolloweb.com/articulos/trabajar-ramas-git.html)

MARKDOWN:
2)
b) 1) lenguaje markdown: “Markdown es realmente dos cosas: por un lado, el lenguaje; por otro, una herramienta de software que convierte el lenguaje en HTML válido.” (https://www.genbeta.com/guia-de-inicio/que-es-markdown-para-que-sirve-y-como-usarlo)
b) 2) Se lo suele utilizar para:
“Markdown nos permite escribir en cualquier lado, en cualquier formato. No tenemos que preocuparnos por equivocarnos por el código, pues escribirlo es simple e intuitivo, y se puede hacer en cualquier parte. Nos permite muchas opciones para personalizar nuestro texto, sin tener que necesariamente visualizarlo. Se puede compartir en cualquier formato y aun así funcionar, y no crear problemas en nuestra web”

”Cuando estamos escribiendo en HTML, para evitar los errores de código de los editores WYSIWYG, muchas veces nos encontramos cometiendo algunos errores básicos que no podemos remediar si estamos apurados. Si nos olvidamos de cerrar una etiqueta, si escribimos mal un enlace, puede estar todo equivocado y nuestra página no se verá bien, en el peor de los casos. HTML puede convivir con algunos errores mínimos, pero no es la idea. Markdown soluciona todo esto” ( https://hipertextual.com/archivo/2013/04/que-es-markdown/)

Ventajas: “usando Markdown no necesitaríamos escribir la etiqueta, sino tan sólo poner una almohadilla al principio:”

“La sintaxis es muy sencilla y cuenta con varias opciones diferentes para algunos de sus elementos”

“Escribir para web es más rápido y cómodo” (https://www.genbeta.com/guia-de-inicio/que-es-markdown-para-que-sirve-y-como-usarlo)
