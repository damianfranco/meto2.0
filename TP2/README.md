# trabajo practico 2


## Encabezados

# encabezado 1

## encabezado 2

### encabezado 3

#### encabezado 4

##### encabezado 5

###### encabezado 6


## Citas

>esto es una cita

>continuacion de cita

## listas

### Desordenadas

- elemento 1
* elemento 2
+ elemento3

### Numeradas

1. elemento 1

2. elemento 2

3. elemento 3

	- elemento 4

## Separaciones

esto es una separacion
___
## Negritas y cursivas

*no se si tengo tiempo quizas me de pronto por vencido y en lugar de eso me eche una siesta.¡Hoy es 18 de junio y es el cumpleaños de miguel!. Una vez quise se astrofisico. Todo el mundo  mundo debe aprender por si mismo a final.*

##### si al mismo texto lo pusiera entre dos astericos se vuelve negrita

**no se si tengo tiempo quizas me de pronto por vencido y en lugar de eso me eche una siesta.¡Hoy es 18 de junio y es el cumpleaños de miguel!. Una vez quise se astrofisico. Todo el mundo  mundo debe aprender por si mismo a final.**

#### y si al texto lo quiero en cursiva y negrita los combino haciendo asi cursiva en negrita

***no se si tengo tiempo quizas me de pronto por vencido y en lugar de eso me eche una siesta.¡Hoy es 18 de junio y es el cumpleaños de miguel!. Una vez quise se astrofisico. Todo el mundo  mundo debe aprender por si mismo a final.***

## Enlaces

[youtube](youtube.com)

##### tambien se puede escribir  un link con entre menor y mayor aunque a mi no me sale nose porque

< https://youtube.com >

## imagenes

[Ebook navegador minimalista](https://enfoquenomada.com/wp-content/upload/2016/07/crear-navegador_minimalistajpg)

[dragon](https://youtube.com)

## Codigo

##### con los tres espacio no me salio el codigo

  Esto es un codigo

#### ahora pruebo con ~(este simbolo)

~~~
dragones y patitos
~~~

###### con este simbolo si salio

## Anular Markdown

se puede hacer de esta forma \*dragones y patito*.

*dragones de patito*