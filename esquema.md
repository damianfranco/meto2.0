## Diagrama UML
### Voy a usar este tipo de diagrama para describir los pasos que hice con los tipos de ramas.

#### Cree una rama llamada tema1

```mermaid
sequenceDiagram
master ->> tema1 : me cambie de la rama master a la de tema1.
Note right of tema1 : cree una carpeta<br/>llamada tema1 y<br/>un archibo llamado<br/>tema1.MD en la<br/>carpeta TP2. 
Note right of tema1 : luego subo carpeta y<br/>archibo a la rama<br/>tema1.
tema1 ->>master : me cambio a la rama Master.
master ->>tema1 : me vuelvo a ir a la rama tema1.
Note right of tema1 : Realizo un cambio al<br/>archibo tema1.MD y<br/>lo subo nuevamente<br/>a la rama tema1.
tema1 ->>master : me cambio a la rama Master. 
Note left of master : y por ultimo una vez<br/>que estoy en la rama<br/>master, realizo un<br/>merge de la rama<br/>tema1.
```


