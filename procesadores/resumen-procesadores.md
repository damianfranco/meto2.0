## Informe de la organizacion basica de una computadora simple
### Descripcion de los componenetes de una CPU
 - **Componentes**:
- Unidad de control
- Alu
- Registros internos
- Memoria principal
- Perifericos de entrada/salida
- Bus de datos

**Descripcion**:
- Unidad de control: La unidad de control se encarga de buscar instrucciones de la memoria principal y determinar su tipo.

- Alu: la unidad de aritmetica y logica realiza operaciones como suma y AND booleando necesarias para ejecutar las instrucciones.

- Registros internos: Por lo regular, todos los registros tienen el mismo tamaño. Cada registro puede contener un numero, hasta algun maximo determinado por el tamaño del registro. Los registros pueden leerse y escribirse a alta velocidad porque estan dentro de la CPU.

- Memoria principal: Es un tipo de memoria pequeña y de alta velocidad que sirve para almacenar resultados temporales y cierta informacion de control.

- Perifericos de entrada/salida: los dispositivos de salida son los que son capaces de reproducir lo que ocurre en la computadora. Los dispostitivos de entrada son los que nos permiten introducir datos.

- Bus de datos: Los componentes estan conectados por medio de ellos. Son una coleccion de alambres paralelos para transmitir que nos permiten transmitir dirrecciones, datos y señales de control.
---
### Explicacion de interaccion de la forma en que los componentes y subcomponentes del sistema interactuan.

- Al iniciar una computadora el control pasa mediante el bus de datos hasta una memoria del tipo ROM, es un tipo de memoria donde se encuentran datos permanentes. Alli comienza la busqueda de la instruccion. Decodifica la instruccion que recibe. Despues de recibir esta informacion, la unidad de control va a mandar la orden de cargar en la memoria principal desde el dispositivo de entrada (disco) los programas del sistema operativo que controlaran las operaciones a seguir. Por ultimo se escribira el resultado de la instruccion del principio en dicho periferico, mientras las demas etapas trabajan con las instrucciones subsecuentes.
